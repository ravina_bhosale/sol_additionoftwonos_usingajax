﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Solution.aspx.cs" Inherits="Sol__AJAX_AdditionOfTwoNos.Solution" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
         <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate>

           <table>
            <tr>
                <td>
                     <asp:TextBox ID="txtVal1" placeHolder="Value1" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtVal2" placeHolder="Value2" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server"></asp:Label>

                    <asp:Button ID="btnSubmit" Text="Addition" runat="server" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        
    </form>
</body>
</html>
