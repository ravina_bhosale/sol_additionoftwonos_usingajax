﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol__AJAX_AdditionOfTwoNos
{
    public partial class Solution : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblResult.Text = Convert.ToString(Convert.ToInt32(txtVal1.Text) + Convert.ToInt32(txtVal2.Text));
        }
    }
}