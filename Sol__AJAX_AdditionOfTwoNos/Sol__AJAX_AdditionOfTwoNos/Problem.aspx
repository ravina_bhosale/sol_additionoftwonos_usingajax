﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Problem.aspx.cs" Inherits="Sol__AJAX_AdditionOfTwoNos.Problem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                     <asp:TextBox ID="txtVal1" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtVal2" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server"></asp:Label>

                    <asp:Button ID="btnSubmit" Text="Addition" runat="server" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
